��          �   %   �      `     a     r     v     {     �  
   �     �  #   �  >   �  4   1  %   f  !   �  /   �  %   �  !     /   &  B   V     �  #   �  !   �      �  %     !   8  /   Z  @   �     �  t  �     ^     n     t     y     �     �  .   �     �  R     N   `  8   �  *   �  9   	  ;   M	  -   �	  <   �	  Q   �	     F
  0   f
  .   �
  -   �
  <   �
  .   1  =   `  R   �  +   �           	                   
                                                                                              Create API Token Key Name Please select the license Profile settings Statistics Users Creating Most Datasets You haven't created any API Tokens. {actor} added the resource {resource} to the dataset {dataset} {actor} added the tag {tag} to the dataset {dataset} {actor} created the dataset {dataset} {actor} created the group {group} {actor} created the organization {organization} {actor} deleted the dataset {dataset} {actor} deleted the group {group} {actor} deleted the organization {organization} {actor} deleted the resource {resource} from the dataset {dataset} {actor} signed up {actor} started following {dataset} {actor} started following {group} {actor} started following {user} {actor} updated the dataset {dataset} {actor} updated the group {group} {actor} updated the organization {organization} {actor} updated the resource {resource} in the dataset {dataset} {actor} updated their profile Project-Id-Version: ckanext-cmo_theme 0.0.1
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2021-12-30 17:15+0000
Last-Translator: 
Language-Team: pt_PT <LL@li.org>
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.7.0
X-Generator: Poedit 2.2.4
 Criar API Token Chave Nome Por favor, selecione a licença Configurações de perfil Estatísticas Utilizadores que criam mais conjuntos de dados Não criou nenhum API Token. O utilizador {actor} adicionou o recurso {resource} ao conjunto de dados {dataset} O utilizador {actor} adicionou a etiqueta {tag} ao conjunto de dados {dataset} O utilizador {actor} criou o conjunto de dados {dataset} O utilizador {actor} criou o grupo {group} O utilizador {actor} criou a organização {organization} O utilizador {actor} eliminou o conjunto de dados {dataset} O utilizador {actor} eliminou o grupo {group} O utilizador {actor} eliminou a organização {organization} O utilizador {actor} eliminou o recurso {resource} do conjunto de dados {dataset} O utilizador {actor} subscreveu O utilizador {actor} começou a seguir {dataset} O utilizador {actor} começou a seguir {group} O utilizador {actor} começou a seguir {user} O utilizador {actor} atualizou o conjunto de dados {dataset} O utilizador {actor} atualizou o grupo {group} O utilizador {actor} atualizou a organização {organization} O utilizador {actor} atualizou o recurso {resource} no conjunto de dados {dataset} O utilizador {actor} atualizou o seu perfil 